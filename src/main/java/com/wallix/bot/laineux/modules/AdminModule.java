package com.wallix.bot.laineux.modules;

import com.wallix.bot.laineux.ChanLine;
import com.wallix.bot.laineux.Laineux;

public class AdminModule extends AbstractLaineuxModule {

	public AdminModule(Laineux laineuxInstance) {
		super(laineuxInstance);
	}

	@Override
	public boolean parse(ChanLine line) {
		if(line.getLine().startsWith("!reload json")) {
			for(ILaineuxModule module : laineuxInstance.getLoadedModules()) {
				if(module instanceof JsonConfigModule) {
					((JsonConfigModule)module).loadJsonConfig(false);
				}
			}
			return true;
		}
		return false;
	}

}
