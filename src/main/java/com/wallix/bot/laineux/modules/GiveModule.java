package com.wallix.bot.laineux.modules;

import com.wallix.bot.laineux.ChanLine;
import com.wallix.bot.laineux.Laineux;

public class GiveModule extends AbstractLaineuxModule {

	public GiveModule(Laineux laineuxInstance) {
		super(laineuxInstance);
	}

	@Override
	public boolean parse(ChanLine line) {
		if(line.isAction() && line.getLine().contains("teste") && line.getLine().contains(Laineux.IRC_NICK)) {
			ChanLine lines[] = new ChanLine[]{
					new ChanLine(true,"se roule en boule"),
					new ChanLine(false,"ronronne"),
			};

			laineuxInstance.writeLines(lines);
		}
		return false;
	}



}
