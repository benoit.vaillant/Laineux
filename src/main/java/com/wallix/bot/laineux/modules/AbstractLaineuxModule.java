package com.wallix.bot.laineux.modules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.wallix.bot.laineux.ChanLine;
import com.wallix.bot.laineux.Laineux;

public abstract class AbstractLaineuxModule implements ILaineuxModule {
	protected Laineux laineuxInstance;

	private Pattern lineSplitter = Pattern.compile(":\\w+![\\w\\.~@]+ PRIVMSG "+Laineux.IRC_CHAN+" :.*");

	public AbstractLaineuxModule(Laineux laineuxInstance) {
		this.laineuxInstance=laineuxInstance;
	}

	@Override
	public void init() {
		// nothing to be done
	}

	@Override
	public boolean rawParse(String line) {
		return false;
	}

	protected ChanLine splitAndStrip(String line) {
		Matcher match = lineSplitter.matcher(line);
		if(match.matches()) {
			return ChanLine.parseLine(match.group(0), match.group(1), match.group(2));
		}
		return null;
	}

}
