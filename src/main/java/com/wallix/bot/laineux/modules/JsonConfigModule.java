package com.wallix.bot.laineux.modules;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.wallix.bot.laineux.ChanLine;
import com.wallix.bot.laineux.Laineux;

public class JsonConfigModule extends AbstractLaineuxModule implements ILaineuxModule {

	private Map jsonMap;
	private Random r = new Random(System.currentTimeMillis());

	private static Pattern userNamePattern = Pattern.compile(".* ([\\w]+) H@? :.*");

	public JsonConfigModule(Laineux laineuxInstance) {
		super(laineuxInstance);
	}

	@Override
	public void init() {
		super.init();
		loadJsonConfig(true);
	}

	@Override
	public boolean parse(ChanLine line) {
		if(jsonMap==null){
			return false;
		}
		Map actions = (Map) ((Map)jsonMap.get("commands")).get("actions");
		Set<String> keys = actions.keySet();
		for(String key:keys) {
			if(line.isAction() && line.getLine().startsWith(key) && line.getLine().contains(Laineux.IRC_NICK)) {
				ArrayList<String> acts = (ArrayList<String>) actions.get(key);
				//chose a random action
				String act = acts.get(r.nextInt(acts.size()));
				Map a = (Map) ((Map)jsonMap.get("actions")).get(act);

				String text = replaceUserString((String)a.get("text"), line.getWho());

				ChanLine repLine = new ChanLine(a.get("type").equals("action"), text);
				laineuxInstance.writeLines(repLine);
				return true;
			}
		}
		if(line.isAction() && line.getLine().endsWith(Laineux.IRC_NICK)) {
			ChanLine repLine = new ChanLine(true, "regarde "+line.getWho()+" avec des gros yeux");
			laineuxInstance.writeLines(repLine);
		}
		return false;
	}

	public void randomAction() {
		if(jsonMap==null){
			return;
		}
		ArrayList actions = (ArrayList) ((Map)jsonMap.get("commands")).get("live");
		String act = (String) actions.get(r.nextInt(actions.size()));
		//System.out.println("** "+act);
		Map a = (Map) ((Map)jsonMap.get("actions")).get(act);

		String text = (String)a.get("text");
		List<String> users = getUsersNotMe();
		if(users.size()==0) {
			return;
		}
		String user = users.get(r.nextInt(users.size()));
		text=replaceUserString(text, user);

		ChanLine repLine = new ChanLine(a.get("type").equals("action"), text);
		laineuxInstance.writeLines(repLine);
	}


	private List<String> getUsersNotMe() {
		ArrayList<String> users = new ArrayList<>();
		laineuxInstance.writeRawLines("WHO "+laineuxInstance.IRC_CHAN);

		try {
			while(true) {
				String line = laineuxInstance.getIrcReader().readLine();
				Matcher m = userNamePattern.matcher(line);
				if(m.matches()) {
					String user = m.group(1);
					if(!user.equals(Laineux.IRC_NICK)) {
						users.add(user);
					}
				}

				if(line.endsWith(":End of WHO list")) {
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return users;
	}

	public void loadJsonConfig(boolean silent) {
		File jsonFile = new File("config.json");

		if(jsonFile.exists()) {
			try {
				JsonParserFactory factory=JsonParserFactory.getInstance();
				JSONParser parser=factory.newJsonParser();
				byte[] encoded;
				encoded = Files.readAllBytes(Paths.get(jsonFile.toURI()));
				jsonMap=parser.parseJson(new String(encoded, Charset.defaultCharset()));

				if(!silent){
					ChanLine success = new ChanLine(false, "Meow !");
					laineuxInstance.writeLines(success);
				}
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(!silent){
			ChanLine error = new ChanLine(false, "Meow ?");
			laineuxInstance.writeLines(error);
		}
	}

	private String replaceUserString(String text, String user) {
		text=text.replace("%(users)s", user);
		text=text.replace("%(user)s", user);
		text=text.replace("%(use)s", user);
		return text;
	}

}
