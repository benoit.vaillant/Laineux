package com.wallix.bot.laineux.modules;

import com.wallix.bot.laineux.ChanLine;

public interface ILaineuxModule {

	public void init();

	public boolean rawParse(String line);

	public boolean parse(ChanLine line);


}
