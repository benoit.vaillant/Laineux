package com.wallix.bot.laineux.modules;

import com.wallix.bot.laineux.ChanLine;

public class EmptyModule implements ILaineuxModule {

	@Override
	public void init() {
	}

	@Override
	public boolean rawParse(String line) {
		return false;
	}

	@Override
	public boolean parse(ChanLine line) {
		return false;
	}

}
