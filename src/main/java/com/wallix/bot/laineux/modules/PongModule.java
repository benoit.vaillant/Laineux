package com.wallix.bot.laineux.modules;

import com.wallix.bot.laineux.ChanLine;
import com.wallix.bot.laineux.Laineux;

public class PongModule extends AbstractLaineuxModule implements ILaineuxModule {

	public PongModule(Laineux laineuxInstance) {
		super(laineuxInstance);
	}

	@Override
	public boolean rawParse(String line) {
		if(line.startsWith("PING ")) {
			System.out.println("Ponging back...");
			return laineuxInstance.writeRawLines("PONG " + line.substring(5));
		}
		return false;
	}

	@Override
	public boolean parse(ChanLine line) {
		return false;
	}

}
