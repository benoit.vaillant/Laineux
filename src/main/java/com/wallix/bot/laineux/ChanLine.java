package com.wallix.bot.laineux;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChanLine {
	private boolean action;
	private String who;
	private String host;
	private String line;

	private static Pattern linePattern = Pattern.compile(":(\\w+)!([\\w~@\\.]+) PRIVMSG "+Laineux.IRC_CHAN+" :(.*)");

	public ChanLine() {
		action=false;
		who="";
		host="";
		line="";
	}

	public ChanLine(boolean action, String line) {
		this.action=action;
		this.line=line;
	}


	public static ChanLine parseLine(String fullRawLine) {
		Matcher m = linePattern.matcher(fullRawLine);
		if(m.matches()) {
			try{
				return parseLine(m.group(1), m.group(2), m.group(3));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChanLine parseLine(String who, String host, String rawLine) {
		ChanLine ret = new ChanLine();
		ret.who=who;
		ret.host=host;
		if(rawLine.startsWith("\u0001ACTION")) {
			ret.action=true;
			rawLine=rawLine.substring(8, Math.max(rawLine.length()-1,8));
		}else {
			ret.action=false;
		}
		ret.line=rawLine;
		return ret;
	}

	public boolean isAction() {
		return action;
	}

	public void setAction(boolean action) {
		this.action = action;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

}
