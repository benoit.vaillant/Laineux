package com.wallix.bot.laineux;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.util.Random;

import com.wallix.bot.laineux.modules.AbstractLaineuxModule;
import com.wallix.bot.laineux.modules.AdminModule;
import com.wallix.bot.laineux.modules.EmptyModule;
import com.wallix.bot.laineux.modules.GiveModule;
import com.wallix.bot.laineux.modules.ILaineuxModule;
import com.wallix.bot.laineux.modules.JsonConfigModule;
import com.wallix.bot.laineux.modules.PongModule;

/**
 * Hello world!
 *
 */
public class Laineux
{

	/** general */
    public static String IRC_HOST="bender.ifr.lan";
    public static int IRC_PORT=6667;
    public static String IRC_CHAN="#a-poils";

    public static String IRC_NICK="laineux";

    /** communication */
    private Socket ircSock;
    private BufferedWriter ircWriter;
    private BufferedReader ircReader;

    /** modules */
    private ILaineuxModule[] loadedModules;

    public static void main( String[] args )
    {
        Laineux laineux = new Laineux();
        laineux.startEngine();
    }


	private void startEngine() {
		if(!connect()) {
			return;
		}

		loadAllModulesInOrder();

		Random r = new Random(System.currentTimeMillis());

		String line;
		// 15 minutes
		int lag=15*60*10;
		int cpt=lag+r.nextInt(lag);
		while(true) {
			try {

				if(ircReader.ready()) {
					line = ircReader.readLine();
					if(line==null) {
						System.out.println("No more lines, exiting");
					}
					parseLine(line);
				}

				Thread.sleep(100);

				cpt--;
				if(cpt<0) {
					cpt=cpt=lag+r.nextInt(lag);
					// do some random action
					for(ILaineuxModule module:loadedModules) {
						if(module instanceof JsonConfigModule) {
							((JsonConfigModule)module).randomAction();
						}
					}
				}
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}


	private boolean connect() {
		try {
			ircSock = new Socket(IRC_HOST, IRC_PORT);

			ircWriter = new BufferedWriter(new OutputStreamWriter(ircSock.getOutputStream()));
	        ircReader = new BufferedReader(new InputStreamReader(ircSock.getInputStream()));

	        ircWriter.write("NICK " + IRC_NICK + "\r\n");
	        ircWriter.write("USER " +IRC_NICK+" 8 * : LaineuxBot\r\n");
	        ircWriter.flush();

	        // Read lines from the server until it tells us we have connected.
	        String line = null;
	        while ((line = ircReader.readLine( )) != null) {
	        	System.out.println("** registering ** "+line);
	            if (line.indexOf("004") >= 0) {
	                // We are now logged in.
	                break;
	            }
	            else if (line.indexOf("433") >= 0) {
	                System.out.println("Nickname is already in use.");
	                return false;
	            }
	        }

	        // Join the channel.
	        ircWriter.write("JOIN " + IRC_CHAN + "\r\n");
	        ircWriter.flush();


		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}


	private void parseLine(String line) {
        // Print the raw line
        System.out.println("[S]"+line);
        for(ILaineuxModule module:loadedModules) {
        	if(module.rawParse(line))
        		return;
        	ChanLine cl = ChanLine.parseLine(line);
        	if(cl!=null) {
        		if(module.parse(cl))
        			return;
        	}
        }
	}

	public boolean writeLines(ChanLine... lines) {
		try {
			for(ChanLine line:lines) {
				if(line.isAction()) {
					System.out.println("[C] * "+line.getLine());
					ircWriter.write("PRIVMSG "+IRC_CHAN+" :\u0001ACTION "+line.getLine()+"\u0001\r\n");
				}else {
					System.out.println("[C] > "+line.getLine());
					ircWriter.write("PRIVMSG "+IRC_CHAN+" :"+line.getLine()+"\r\n");
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			ircWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean writeRawLines(String... lines) {
		try {
			for(String line:lines) {
				System.out.println("[C] "+line);
				ircWriter.write(line+"\r\n");
			}
			ircWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void loadAllModulesInOrder() {
		loadModules(PongModule.class, AdminModule.class, JsonConfigModule.class, GiveModule.class);
	}


	private void loadModules(Class<? extends AbstractLaineuxModule>... modules) {
		loadedModules=new ILaineuxModule[modules.length];
		for (int i = 0; i < modules.length; i++) {
			try {
				loadedModules[i]=modules[i].getDeclaredConstructor(Laineux.class).newInstance(this);
				loadedModules[i].init();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				loadedModules[i]=new EmptyModule();
				e.printStackTrace();
			}
		}
	}

	public ILaineuxModule[] getLoadedModules() {
		return loadedModules;
	}

	public BufferedReader getIrcReader() {
		return ircReader;
	}

}
